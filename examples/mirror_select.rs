extern crate weighted_random_list;

use weighted_random_list::WeightedRandomList;

fn main() {
    let list = [
        (1, "https://source.example.net/archive"),
        (10, "https://mirror-slow0.example.net/archive"),
        (10, "https://mirror-slow1.example.net/archive"),
        (100, "https://mirror-fast.example.net/archive"),
    ];

    let mirrors = list.iter()
            .map(|(weight, url)| (*weight, url.to_string()))
            .collect::<WeightedRandomList<String>>();


    let random_choice = mirrors.get_random();
    println!("Using {:?} this time", random_choice);
}
