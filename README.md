# weighted_random_list

[![CI Status](https://gitlab.com/dns2utf8/weighted_random_list/badges/master/build.svg)](https://gitlab.com/dns2utf8/weighted_random_list)
[![coverage report](https://gitlab.com/dns2utf8/weighted_random_list/badges/master/coverage.svg)](https://gitlab.com/dns2utf8/weighted_random_list/commits/master)

A Vec<T> that allows you to define the weight of each entry and randomly get entries:

```rust
extern crate weighted_random_list;

use weighted_random_list::WeightedRandomList;

fn main() {
    let list = [
        (1, "https://source.example.net/archive"),
        (10, "https://mirror-slow0.example.net/archive"),
        (10, "https://mirror-slow1.example.net/archive"),
        (100, "https://mirror-fast.example.net/archive"),
    ];

    let mirrors = list.iter()
            .map(|(weight, url)| (*weight, url.to_string()))
            .collect::<WeightedRandomList<String>>();


    let random_choice = mirrors.get_random();
    println!("Using {:?} this time", random_choice);
}
```
